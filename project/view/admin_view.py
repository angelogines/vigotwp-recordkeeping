# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Admin Page"""

from PyQt5.QtWidgets import (QComboBox,
                                                   QDateEdit,
                                                   QFormLayout,
                                                   QHBoxLayout,
                                                   QLabel,
                                                   QLineEdit,
                                                   QPushButton,
                                                   QVBoxLayout,
                                                   QWidget,
                                                   )
from PyQt5.QtCore import (pyqtSlot,
                                             QObject,
                                             QPoint,
                                             Qt,
                                             QRegExp)
from PyQt5.QtGui import (QRegExpValidator)
#from classes.main_view import MainWindow



class AdminViewTab(QWidget):
    def __init__(self, parent=None):
        super(AdminViewTab, self).__init__(parent=parent)

        # Define the base layout
        self.layout_base = QHBoxLayout()

        # Define the layout for the buttons.
        layout_buttons = QVBoxLayout()

        widget_buttons = QWidget()

        # Following the principle above, I will create a blank widget.
        # This blank widget will then be populated with the forms needed to
        # complete the admin functions
        widget_work_area = QWidget()

        # Define the buttons for the administrator functions
        self.button_add_user = QPushButton('Add New Personnel', self)
        self.button_update_user = QPushButton('Update Personnel Information')
        self.button_list_roster = QPushButton('List Current Roster')

        # Define the signals for the administrator buttons
        self.button_add_user.clicked.connect(self.show_add_new_user_form)

        # Add the buttons to the buttons layout
        layout_buttons.addWidget(self.button_add_user)
        layout_buttons.addWidget(self.button_update_user)
        layout_buttons.addWidget(self.button_list_roster)

        # add the buttons layout to this widget, for some reason.
        widget_buttons.setLayout(layout_buttons)

        # Add the widget with the buttons to the base layout
        self.layout_base.addWidget(widget_buttons)

        # Add the work area Widget to the base layout
        self.layout_base.addWidget(widget_work_area)

        self.setLayout(self.layout_base)

    @pyqtSlot()
    def show_add_new_user_form(self):
        # print("This will display the new user form")

        # Remove any widgets that might be in the
        myWidget = self.layout_base.itemAt(1).widget()
        myWidget.setParent(None)
        self.layout_base.removeWidget(myWidget)

        self.layout_base.addWidget(AddNewUserForm())

class AddNewUserForm(QWidget):
    def __init__(self, parent=None):
        super(AddNewUserForm, self).__init__(parent=parent)

        form_layout = QFormLayout()
        form_layout.addRow(QLabel("Personnel Information"))
        form_layout.addRow(QLabel('* Denotes Required Field'))

        call_sign = QLineEdit()
        form_layout.addRow("Call Sign", call_sign)

        rank = QComboBox()
        rank.addItem("")
        rank.addItem("Needs Database")
        rank.addItem("Connect to Database")
        form_layout.addRow("Rank", rank)

        first_name = QLineEdit()
        form_layout.addRow("* First Name", first_name)

        middle_name = QLineEdit()
        form_layout.addRow("Middle Name", middle_name)

        last_name = QLineEdit()
        form_layout.addRow("* Last Name", last_name)

        date_of_birth = QDateEdit()
        form_layout.addRow("* Date of Birth", date_of_birth)

        address_line_one = QLineEdit()
        form_layout.addRow("* Address Line One", address_line_one)

        address_line_two = QLineEdit()
        form_layout.addRow("Address Line Two", address_line_two)

        zip_code = QLineEdit()
        zip_validation_code = QRegExp('^\d{5}(-\d{4})?$')
        zip_validator = QRegExpValidator(zip_validation_code, zip_code)
        zip_code.setValidator(zip_validator)
        form_layout.addRow("* Zip Code", zip_code)

        user_telephone_number = QLineEdit()
        user_telephone_number.setInputMask("(999)999-9999")
        form_layout.addRow("Telephone Number", user_telephone_number)

        email_address = QLineEdit()
        email_validation_code =  QRegExp("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", Qt.CaseInsensitive)
        email_validator = QRegExpValidator(email_validation_code, email_address)
        email_address.setValidator(email_validator)
        form_layout.addRow("Email Address", email_address)

        username = QLineEdit()
        form_layout.addRow("* Username", username)

        password = QLineEdit()
        password.setEchoMode(QLineEdit.Password)
        form_layout.addRow("* Password", password)

        form_layout.addRow(QLabel("Emergency Contact Information"))

        emergency_first_name = QLineEdit()
        form_layout.addRow("First Name", emergency_first_name)

        emergency_last_name =QLineEdit()
        form_layout.addRow("Last Name", emergency_last_name)

        emergency_telephone_number = QLineEdit()
        emergency_telephone_number.setInputMask("(999)999-9999")
        form_layout.addRow("Telephone Number", emergency_telephone_number)

        button_submit = QPushButton("Submit")
        button_cancel = QPushButton("Cancel")
        form_layout.addRow(button_submit, button_cancel)


        self.setLayout(form_layout)



    def createWidget(self, parent):
        return AddNewUserForm(parent)


    # @pyqtSlot
    # define functions
    @pyqtSlot()
    def on_button_submit_clicked(self):
        print("Information Sent to Database")
        pass

    @pyqtSlot()
    def on_button_cancel_clicked(self):
        print("Form cleared and reset")
        pass


#class AdminView(QWidget):
#
#    """    Provides the view for the admin functions.    """
#
#    def __init__(self, model, main_controller):
#        """
#        Constructor
#
#        @param parent reference to the parent widget
#        @type QWidget
#        """
#        super().__init__()
#        self._model = model
#        self._main_controller = main_controller
#        label = QLabel("Admin View Page")
#        label.setAlignment(Qt.AlignCenter)


        # connect widgets to controller
        # See https://stackoverflow.com/questions/26698628/mvc-design-with-qt-designer-and-pyqt-pyside/26699122#26699122
        # for examples of what to do
        #
        # Another website with examples:
        # https://www.reddit.com/r/learnpython/comments/7w9pt9/pyqt5_passing_variable_from_one_window_to_another/

        # listen for model event signals

    # @pyqtSlot
    # define functions

