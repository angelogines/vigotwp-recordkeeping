# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


"""Main Window module."""

from PyQt5.QtWidgets import QMainWindow, QLabel
from PyQt5.QtCore import Qt


class MainView(QMainWindow):

    """Main Window Class."""

    def __init__(self, *args, **kwargs):
        """Initializer."""
        super(MainView, self).__init__(*args, **kwargs)

        self.setWindowTitle("Vigo Twp Fire Dept Recordkeeping")
        label = QLabel("Vigo Township Fire Department")
        label.setAlignment(Qt.AlignCenter)

        self.setCentralWidget(label)
