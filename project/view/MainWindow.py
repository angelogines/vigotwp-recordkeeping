from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.setWindowTitle("Vigo Twp Fire Dept Recordkeeping")
        label = QLabel("Vigo Township Fire Department")
        label.setAlignment(Qt.AlignCenter)

        self.setCentralWidget(label)
