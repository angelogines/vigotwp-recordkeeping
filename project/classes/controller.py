# coding=UTF-8
# MIT License
#
# Copyright (c) 2019 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
Controller Module.

Provides the controller functions in the MVC paradigm.
This was supposed to be a parent module from
which other controllers could inherit.

"""

from project.classes.model import Model
# from classes.model import Model
# from classes.View import View
from project.classes.view import View

class Controller:

    """    Provides the controller of model-View-Controller.    """

    def __init__(self):
        """        Initializer.        """
        self.model = Model()
        self.view = View(self)

    def main(self):
        """    main function.    """
        self.view.main()
