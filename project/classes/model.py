# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""    Provides the Model of the model-view-controller paradigm.    """


# import project.classes.mvc_exceptions as mvc_exc
import hashlib
import binascii
import os

from PyQt5.QtCore import QObject, pyqtSignal


# This method was borrowed from
# https://www.vitoshacademy.com/hashing-passwords-in-python/
def hash_password(password: str) -> str:
    """
    Input raw password return hashed password.

    Input a raw password (string).
    Return a hashed and salted password (string).

    """

    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


# This method was borrowed from:
# https://www.vitoshacademy.com/hashing-passwords-in-python/
def verify_password(stored_password: str,
                    provided_password: str) -> bool:
    """
    Verify a stored password against one provided by user.

    Returns a boolean.

    """

    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password



class Model(QObject):

#    """    Provides the model of Model-View-Controller.    """
#    enable_admin_func_clicked = pyqtSignal()
#
#    @property
#    def enable_admin_func_page(self):
#        return self._enable_admin_func_page
#
#    @enable_admin_func_page.setter
#    def enable_admin_func_page(self):
#        #self._enable_admin_func_page = value
#        self.enable_admin_func_clicked.emit()


    def __init__(self):
        """        Initializer.        """
        super().__init__()
        # self._enable_admin_func_page = False


