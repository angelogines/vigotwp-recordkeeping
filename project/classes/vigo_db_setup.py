# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""Database schema module."""

import sqlalchemy
# import sqlalchemy.orm
from sqlalchemy import (Column, String, Integer, ForeignKey,
                        DateTime, Boolean, BLOB, Date, func,
                        Unicode)
from sqlalchemy_continuum import make_versioned
from sqlalchemy_utils import force_auto_coercion
from sqlalchemy_utils import EncryptedType, PasswordType

from project.classes.base import Base


# Make the tables "versioned" as per sqlalchemy_continuum
make_versioned(user_cls=None)

# Force auto_coercion as per sqlalchemy_utils
force_auto_coercion()

# This will need to be changed.  We will need to determine a method
# of producing, storing, and retrieving the key.
# TODO:  Determine the best way to handle this key
# NOTE:
get_key = "This is a really insecure key"


class Personnel(Base):

    """
    Class to represent personnel on the fire dept.

    This class will hold information pertinent to the members of the
    department.
    This table is set up to track all changes made, when they were made,

    and who made those changes.

    """

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel"

    def __repr__(self) -> str:
        """Return a text representation of Personnel."""
        return ('<Personnel(personnel_id={}, changed_by={}, callsign={},'
                'first_name={}, middle_name={}, last_name={},'
                'date_of_birth={}, social_security_number={},'
                'address_line_one={}, address_line_two={}, zip_code={},'
                'telephone={}, personnel_email={}, username={},'
                'is_password_expired={}, emergency_contact_first_name={},'
                'emergency_contact_last_name={},'
                'emergency_contact_telephone={})>'
                .format(self.personnel_id,
                        self.changed_by, self.callsign, self.first_name,
                        self.middle_name, self.last_name, self.date_of_birth,
                        self.social_security_number, self.address_line_one,
                        self.address_line_two, self.zip_code, self.telephone,
                        self.personnel_email, self.username,
                        self.is_password_expired,
                        self.emergency_contact_first_name,
                        self.emergency_contact_last_name,
                        self.emergency_contact_telephone
                        )
                )

    def __str__(self) -> str:
        """Return a string representation of Personnel."""
        return ('<Personnel(personnel_id={}, changed_by={}, callsign={}, '
                'first_name={}, middle_name={}, last_name={}, '
                'date_of_birth={}, social_security_number={}, '
                'address_line_one={}, address_line_two={}, zip_code={}, '
                'telephone={}, personnel_email={}, username={}, '
                'is_password_expired={}, emergency_contact_first_name={}, '
                'emergency_contact_last_name={}, '
                'emergency_contact_telephone={})>'
                .format(self.personnel_id,
                        self.changed_by, self.callsign, self.first_name,
                        self.middle_name, self.last_name, self.date_of_birth,
                        self.social_security_number, self.address_line_one,
                        self.address_line_two, self.zip_code, self.telephone,
                        self.personnel_email, self.username,
                        self.is_password_expired,
                        self.emergency_contact_first_name,
                        self.emergency_contact_last_name,
                        self.emergency_contact_telephone
                        )
                )
    # Internal ID number
    personnel_id = Column(Integer, primary_key=True, nullable=False,
                          autoincrement=True)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                        nullable=False)

    # The call-sign by which this person is known.  For example, Chief is 17-01
    callsign = Column(String)

    # Person's legal first name
    first_name = Column(String, nullable=False)

    # legal middle name
    middle_name = Column(String)

    # legal last name
    last_name = Column(String, nullable=False)

    # Date on which this person was born
    date_of_birth = Column(Date, nullable=False)

    # Person's social security number.
    social_security_number = Column(EncryptedType(Unicode, get_key))

    # Person's mailing address.
    address_line_one = Column(String, nullable=False)
    address_line_two = Column(String)
    # Person's Zip code.   From the Zip code, we can deduce city
    zip_code = Column(String, nullable=False)

    # person's telephone number
    telephone = Column(String)

    # Personnel's email address
    personnel_email = Column(String)

    # Username used to log into this system
    username = Column(String, nullable=False)

    # Password which will be stored as a hash
    password = Column(PasswordType(
        schemes=[
            'pbkdf2_sha512',
            'md5_crypt'
        ],
        deprecated=['md5_crypt']
    ), nullable=False)

    # If this user's password is expired,
    # the user will be prompted to create a new password
    is_password_expired = Column(Boolean, unique=False, default=False)

    # Information of someone to contact in case of an emergency involving
    # this person.
    emergency_contact_first_name = Column(String)
    emergency_contact_last_name = Column(String)
    emergency_contact_telephone = Column(String)


class CertificationType(Base):

    """
    Class to track certifications.

    This class is designed to track certifications such as:
    CPR
    HazMat
    FireFighter modules
    and other similar certs.

    """

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "certification_type"

    def __repr__(self) -> str:
        """Return a text representation of CertificationType."""
        return ('<CertificationType(certification_id={},'
                'changed_by={}, certification_name={},'
                'issuing_agency={})>'
                .format(self.certification_id, self.changed_by,
                        self.certification_name,
                        self.issuing_agency))

    def __str__(self) -> str:
        """Return a string description of Certification Type."""
        return ('Certification Type is {}.  It is issued through {}'
                .format(self.certification_name, self.issuing_agency))

    # Internal ID number
    # Issued by SQLAlchemy
    certification_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making this entry
    changed_by = Column(Integer, ForeignKey(
        'personnel.personnel_id'), nullable=False)

    # Certification specifics
    certification_name = Column(String)
    issuing_agency = Column(String)


class PersonnelCertification(Base):

    """
    Linking table to link Certifications to Personnel.

    This class will track which certifications have been issued to
    which personnel, the date of issue, the date of expiration (if any),
    and certification numbers.

    This will also include drivers license and other similar documents

    """

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel_certification"

    def __repr__(self) -> str:
        """Return a string representation of PersonnelCertification."""
        return('personnel_certification_id = {}, changed_by = {}'
               'personnel_id = {}, certification_id = {},'
               'initial_cert_date = {}, expire_cert_date = {},'
               'cert_number = {}'
               .format(self.personnel_certification_id, self.changed_by,
                       self.personnel_id, self.certification_id,
                       self.initial_cert_date, self.expire_cert_date,
                       self.cert_number))

    def __str__(self) -> str:
        """Return a human readable representation of PersonnelCertification."""
        return self.__repr__

    # Internal ID number
    personnel_certification_id = Column(Integer, primary_key=True,
                                        nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(),
                                nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer,
                        ForeignKey('personnel.personnel_id'),
                        nullable=False)

    # ID of the member who holds this specific cert
    personnel_id = Column(Integer,
                          ForeignKey('personnel.personnel_id'),
                          nullable=False)

    # ID of the certification
    certification_id = Column(Integer, ForeignKey
                              ('certification_type.certification_id'),
                              nullable=False)

    # Certification Dates
    initial_cert_date = Column(DateTime)
    expire_cert_date = Column(DateTime)

    # Document number of the certifcation, if it has one.
    cert_number = Column(String)

    # Scan or photo of the Certification
    cert_scan = Column(BLOB)


class PersonnelAllergies(Base):

    """Class to track medical allergies of personnel."""

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel_allergies"

    def __repr__(self) -> str:
        """Return a string representation."""
        return('self.personnel_allergies_id = {},'
               'self.changed_by = {}, self.allergy_name = {},'
               'self.personnel_id = {}'
               .format(self.personnel_allergies_id, self.changed_by,
                       self.allergy_name, self.personnel_id))

    def __str__(self) -> str:
        """Return a string representation."""

        return self.__repr__()

    # Internal ID number
    personnel_allergies_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer,
                        ForeignKey('personnel.personnel_id'), nullable=False)

    allergy_name = Column(String)
    personnel_id = Column(Integer, ForeignKey('personnel.personnel_id'))


class Discipline(Base):

    """
    Class to track disciplinary actions.

    This class is designed to track types of disciplinary actions
    available.

    """

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "discipline"

    def __repr__(self) -> str:
        """Return a string."""
        return ('self.discipline_id = {}, self.changed_by = {},'
                'discipline_name = {}, discipline_description = {}'
                .format(self.discipline_id, self.changed_by,
                        self.discipline_name, self.discipline_description))

    def __str__(self):
        """Return a string."""
        return self.__repr__()

    # Internal ID number
    discipline_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(),
                                nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                        nullable=False)

    # Disciplinary action taken or a short description
    # example would be: termination, suspension, retraining, verbal reprimand
    discipline_name = Column(String, nullable=False)

    # A description of the actions that led up to the descipline
    discipline_description = Column(String, nullable=False)


class PersonnelDiscipline(Base):

    """Linking Class to link Personnel to Discipline."""

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel_discipline"
    # Internal ID number
    personnel_discipline_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer,
                        ForeignKey('personnel.personnel_id'),
                        nullable=False)

    # Date of disciplinary action
    discipline_date = Column(DateTime, nullable=False)

    # Who delivered the discipline?
    discipline_by = Column(Integer,
                           ForeignKey('personnel.personnel_id'),
                           nullable=False)

    # Recipient of Discipline
    discipline_recipient = Column(Integer,
                                  ForeignKey('personnel.personnel_id'),
                                  nullable=False)

    # Free form text to keep notes of the discipline
    notes = Column(String)


class PersonnelEmploymentDate(Base):

    """
    Class to track the beginning and ending employment dates.

    As a person may join and subsequently leave the service of the fire
    department more than once, it is, in this author's opinion,
    imperative to keep a complete record of such actions.

    """

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel_employement_date"
    # Internal ID number
    personnel_employment_date_id = Column(Integer, primary_key=True,
                                          nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer,
                        ForeignKey('personnel.personnel_id'), nullable=False)

    hire_date = Column(DateTime)
    termination_date = Column(DateTime)

    # Notes on the reason behind the termination of the employment
    discharge_disposition = Column(String)

    # The employee / personnel who is hired / terminated
    personnel_id = Column(Integer,
                          ForeignKey('personnel.personnel_id'), nullable=False)


class PersonnelEquipment(Base):

    """
    Track who has what equipment.

    As a person will have more than one piece of equipment
    asssigned to him and over the course of time may have multiple
    pieces of the same type of equipment.

    """

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel_equipment"
    # Internal ID number
    personnel_equipment_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                        nullable=False)

    receipient = Column(Integer, ForeignKey('personnel.personnel_id'),
                        nullable=False)
    equipment = Column(Integer, ForeignKey('equipment.equipment_id'),
                       nullable=False)
    date_of_issue = Column(DateTime)

    # TODO:  I think we need to add a table or perhaps an enum to limit the
    # condition descriptions
    issue_condition = Column(String)

    date_of_return = Column(DateTime)
    return_condition = Column(String)

    note = Column(String)


class Equipment(Base):

    """
    Class to track equipment inventory.

    This class is designed to track equipment inventory.

    """

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "equipment"
    # Internal ID number
    equipment_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(),
                                nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer,
                        ForeignKey('personnel.personnel_id'), nullable=False)

    # Name of the equipment
    name = Column(String, nullable=False)

    # Description of the equipment
    description = Column(String, nullable=False)
    manufacturer = Column(String)
    model_number = Column(String)
    serial_number = Column(String)
    purchase_date = Column(DateTime, nullable=False)

    # Purchase price is in pennies.  To get dollars, you will need to divide
    # purchase price by 100.
    purchase_price = Column(Integer)

    # When was the equipment retired from service
    out_of_service_date = Column(DateTime)

    # Pictures of the equipment
        # Need to set up a table for pictures and link it from here
        # to there. There should be other tables that will need
        # access to the pictures table.


class EquipmentInspection(Base):

    """
    Class to track equipment inspections.

    This class is designed to track equipment inspections.

    """

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "equipment_inspection"
    # Internal ID number
    equipment_inspection_id = Column(Integer, primary_key=True, nullable=False)

    # Determine's if this entry is the most up to date.
    # is_current_version = Column(Boolean, unique=False, default=False,
    #                            primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                        nullable=False)

    # Foriegn Key to the Equipment
    equipment_id = Column(Integer, ForeignKey('equipment.equipment_id'),
                          nullable=False)

    # date of inspection
    inspection_date = Column(DateTime)

    # Equipment inspection Pass / Fail
    did_pass_inspection = Column(Boolean, unique=False, default=True)

    # Who did the inspection
    inspected_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                          nullable=False)

    # Condition of the equipment at the time of inspection
        # Can this be incorporated with the Pass / Fail?
    equipment_condition = Column(String)


class Medication(Base):

    """
    Class to maintain a list a medications.

    This should result in a more consistent med list
    and spellings.

    """

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "medication"
    # Internal ID number
    medication_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any changes to
    # this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                           nullable=False)

    medication_name = Column(String, nullable=False)


class PersonnelMedication(Base):

    """Linking Class/Table to track which personnel is on what medications."""

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel_medication"
    # Internal ID number
    personnel_medication_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                          nullable=False)

    medication = Column(Integer, ForeignKey
                        ('medication.medication_id'), nullable=False,
                        primary_key=True)
    personnel = Column(Integer, ForeignKey
                       ('personnel.personnel_id'), nullable=False,
                       primary_key=True)


class MedicalCondition(Base):

    """List of medical conditions."""

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "medical_condition"
    # Internal ID number
    condition_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                         nullable=False)

    condition_name = Column(String, nullable=False)


class PersonnelMedicalHistory(Base):

    """Linking Class / Table to track personnel medical history."""

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel_medical_history"
    # Internal ID number
    personnel_medical_history_id = Column(Integer, primary_key=True,
                                                nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                          nullable=False)

    condition = Column(Integer, ForeignKey
                       ('medical_condition.condition_id'), nullable=False,
                       primary_key=True)
    personnel = Column(Integer, ForeignKey
                       ('personnel.personnel_id'), nullable=False,
                       primary_key=True)


class Training(Base):

    """Record Training Events."""

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "training"
    # Internal ID number
    training_id = Column(Integer, primary_key=True, nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
                        nullable=False)

    start_datetime = Column(DateTime)
    end_datetime = Column(DateTime)
    training_subject = Column(String)

    # TODO: I had orignially planned on this being a foriegn key back to
    # personnel; however, I failed to take into account guest speakers.
    instructor = Column(String)


class PersonnelTraining(Base):

    """Link Training with Personnel."""

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel_training"
    # Internal ID number
    personnel_medical_history_id = Column(Integer, primary_key=True,
                                          nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
        nullable=False)

    training = Column(Integer, ForeignKey
                      ('training.training_id'), nullable=False,
                      primary_key=True)
    personnel = Column(Integer, ForeignKey
                       ('personnel.personnel_id'), nullable=False,
                       primary_key=True)

class Rank(Base):

    """List of available Ranks."""

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "rank"
    # Internal ID number
    rank_id = Column(Integer, primary_key=True,
                                          nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
        nullable=False)

    # The name of this rank.  Example:  Chief, Assistant Chief, Captain
    rank_name = Column(String)

    rank_description = Column(String)

class PersonnelRank(Base):

    """Link Rank with Personnel."""

    def __repr__(self):
        """Return a __repr__."""
        return str(self.__dict__)

    def __str__(self):
        """Return a string."""
        return str(self.__dict__)

    __versioned__ = {}  # Needed for sqlalchemy_continuum
    __tablename__ = "personnel_rank"
    # Internal ID number
    personnel_rank_id = Column(Integer, primary_key=True,
                                          nullable=False)

    # Date-Time when this entry was made
    inserted_timestamp = Column(DateTime, default=func.now(), nullable=False)

    # This will be the user ID who is responsible for making any
    # changes to this entry
    changed_by = Column(Integer, ForeignKey('personnel.personnel_id'),
        nullable=False)

    training = Column(Integer, ForeignKey
                      ('rank.rank_id'), nullable=False,
                      primary_key=True)
    personnel = Column(Integer, ForeignKey
                       ('personnel.personnel_id'), nullable=False,
                       primary_key=True)

# after you have defined all your models, call configure_mappers:
sqlalchemy.orm.configure_mappers()
