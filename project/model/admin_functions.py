# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""    This will be the model for administrator functions.    """



import project.classes.vigo_db_setup
from project.classes.base import session_factory
from project.classes.model import Model

# This will be used to determine who the currently logged in user is
# to track who is responsible for making the changes in the database.
# Currently, this functionality has not been implemented so for now,
# I am setting this to a default user of "0"
current_user = 0


class Administrator(Model):

    """This class will provide administrator functions."""

    def __init__(self):
        """initializer."""

    def add_new_personnel(self, **kwargs) -> str:
        """        Method used to add a new person to the system.        """

        session = session_factory()

        # The "changed_by" attribute will need to be changed in the
        # future.
        # It really needs to be set equal to the id of the person
        # currently logged and making the changes, but that
        # functionality has not yet been implimented.

        new_person = project.classes.vigo_db_setup.Personnel(
            changed_by=current_user,
            callsign=kwargs.get('callsign'),
            first_name=kwargs.get('first_name'),
            middle_name=kwargs.get('middle_name'),
            last_name=kwargs.get('last_name'),
            date_of_birth=kwargs.get('date_of_birth'),
            social_security_number=kwargs.get('social_security_number'),
            address_line_one=kwargs.get('address_line_one'),
            address_line_two=kwargs.get('address_line_two'),
            zip_code=kwargs.get('zip_code'),
            telephone=kwargs.get('telephone'),
            personnel_email=kwargs.get('personnel_email'),
            username=kwargs.get('username'),
            password=kwargs.get('password'),
            is_password_expired=kwargs.get('is_password_expired'),
            emergency_contact_first_name=kwargs
                .get('emergency_contact_first_name'),

            emergency_contact_last_name=kwargs
                .get('emergency_contact_last_name'),

            emergency_contact_telephone=kwargs
                .get('emergency_contact_telephone')
        )

        session.add(new_person)
        session.commit()
        session.close()

    def update_personnel_info(self, personnel_id, **kwargs):
        """Method to update personnel information."""

        session = session_factory()
        info = session.query(project.classes
                             .vigo_db_setup.Personnel).filter(project.classes
                                                              .vigo_db_setup
                                                              .Personnel
                             .personnel_id
                                                == personnel_id).first()

        # Now I need to determine which attribute(s) need changed.
        for column in info.__table__.columns:
            for row_kwargs in kwargs:
                if column.name == row_kwargs:
                    # if the rows are the same, update info with the value of
                    # kwargs
                    setattr(info, column.name, kwargs.get(row_kwargs))

        # import pdb; pdb.set_trace()

        session.add(info)
        session.commit()
        session.close()

    def add_new_cert_type(self, **kwargs):
        """
        Add new certification type.

        method to maintain information about the different types
        of certifications a member can hold.

        """

        session = session_factory()

        new_cert_type = project.classes.vigo_db_setup.CertificationType(
            changed_by=current_user,
            certification_name=kwargs.get('certification_name'),
            issuing_agency=kwargs.get('issuing_agency')
        )

        session.add(new_cert_type)
        session.commit()
        session.close()


    # Read all data about all personnel
        # This should be a simple database read.
        # The complexity will come when we have to sort the data based on
        # different criteria.

    # remove person from active duty
        # If this person has any equipment assigned to him, then he should not be able
            # to be removed from service until all the equipment has been returned.
        # If all equipment and similar checks have cleared, then do the following.
            # Set PersonnelEmploymentDate.discharge_date = date discharge was active
