##!/usr/bin/env python3
# -*- coding: utf-8 -*-
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# metadata
"Vigo Twp is a small volunteer fire dept."
__version__ = "0.1"
__license__ = "OSI Approved/MIT License"
__author__ = "The Wise Baizes Programming Group"
__email__ = "vigotwp_rkp@zoho.com"
__url__ = "http://gitlab.com/Pavulon18/vigotwp-recordkeeping"
__date__ = "2019-11-07T18:30:31"
__prj__ = "VigoTwp RecordKeeping"

from setuptools import setup, find_packages


setup(
    name="VigoTwp RecordKeeping",
    version="0.1",
    description="Vigo Twp is a small volunteer fire dept.",
    long_description="""Vigo Twp is a small volunteer fire dept.  This project will assist in record keeping of assets, personnel, state reports, and more.""",
    author="The Wise-Baizes Programming Group",
    author_email="vigotwp_rkp@zoho.com",
    maintainer="Author",
    maintainer_email="Author",
    url="http://gitlab.com/Pavulon18/vigotwp-recordkeeping",
    classifiers=[
        "License :: OSI Approved :: MIT License"
    ],
    packages=find_packages(),
    include_package_data = True,
)
