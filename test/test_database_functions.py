# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
Verify the ability to connect to a dataase.

This test is here to verify that the database is correctly setup
and can complete CRUD operations.


"""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import project.classes.vigo_db_setup
from datetime import date
import pytest
import sqlalchemy


# global application scope.  create Session class, engine
# engine = create_engine('sqlite:///test_db_func.db', echo=True)
engine = create_engine('sqlite:///:memory:', echo=True)
Session = sessionmaker(bind=engine)
session = Session()
project.classes.vigo_db_setup.Base.metadata.create_all(engine)


class TestDatabase():
    def setUp(self):
        # connect to the database
        self.connection = engine.connect()

        # begin a non-ORM transaction
        self.trans = self.connection.begin()

        # bind an individual Session to the connection
        self.session = Session(bind=self.connection)

    def test_name_retrieval(self):
        #  This writes directly to and reads from the database.
        #  This will ensure that there is a valid connection to the database

        test_db = (project.classes.vigo_db_setup.Personnel(
            changed_by=0,
            first_name='Clinton',
            last_name='Kildepstein',
            date_of_birth=date(1900, 1, 15),
            address_line_one='1600 Pennsylvania Ave',
            zip_code='86210',
            username='secure',
            password='server'
        ))
        session.add(test_db)
        session.commit()
        session.close()

        result = session.query(project.classes.vigo_db_setup.Personnel).first()
        assert result.first_name == 'Clinton'

    def test_is_personnel_id_nullable_false(self):
        test_db_one = project.classes.vigo_db_setup.Personnel(
                personnel_id=None,
                changed_by=0,
                first_name='Clinton',
                middle_name='One',
                last_name='Kildepstein',
                date_of_birth=date(1920, 1, 15),
                address_line_one='1600 Pennsylvania Ave',
                zip_code='86210',
                username='secure',
                password='server'
                )
        session.add(test_db_one)
        session.commit()
        session.close()
        result = session.query(project.classes.vigo_db_setup.Personnel).first()
        assert result.personnel_id is not False

    def test_is_personnel_inserted_timestamp_nullable_false(self):
        test_db_two = project.classes.vigo_db_setup.Personnel(
                changed_by=0,
                inserted_timestamp=None,
                first_name='Clinton',
                middle_name='Two',
                last_name='Kildepstein',
                date_of_birth=date(1920, 1, 15),
                address_line_one='1600 Pennsylvania Ave',
                zip_code='86210',
                username='unsecured',
                password='server'
                )
        session.add(test_db_two)
        session.commit()
        session.close()
        result = session.query(project.classes.vigo_db_setup.Personnel).first()
        assert result.inserted_timestamp is not False

    def test_is_personnel_changed_by_nullable_false(self):
        with pytest.raises(sqlalchemy.exc.IntegrityError):
            test_db_three = project.classes.vigo_db_setup.Personnel(
                       changed_by=None,
                        first_name='Clinton',
                        middle_name = 'Three',
                        last_name='Kildepstein',
                        date_of_birth=date(1920, 1, 15),
                        address_line_one='1600 Pennsylvania Ave',
                        zip_code='86210',
                         username='unsecured',
                       password='server'
            )
            session.add(test_db_three)
            session.commit()
        session.close()

    def test_is_personnel_first_name_false(self):
        with pytest.raises(sqlalchemy.exc.IntegrityError):
            test_db_four = project.classes.vigo_db_setup.Personnel(
                    first_name=None,
                    middle_name = 'Four',
                    last_name='Kildepstein',
                    date_of_birth=date(1920, 1, 15),
                    address_line_one='1600 Pennsylvania Ave',
                    zip_code='86210',
                    username='unsecured',
                    password='server'
            )
            session.add(test_db_four)
            session.commit()
        session.close()

    def tearDown(self):
        # self_session.commit()
        self.session.flush()
        self.session.close()

        # rollback - everything that happened with the
        # Session above (including calls to commit())
        # is rolled back.
        # self.trans.rollback()

        # return connection to the Engine
        self.connection.close()
