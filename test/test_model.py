# coding=UTF-8
# MIT License
#
# Copyright (c) 2019, 2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Module to test the classes and methods used in the base model."""


import project.classes.vigo_db_setup
import project.classes.model
import project.model.admin_functions
from project.classes.base import session_factory


# global application scope.  create Session class, engine


session = session_factory()


class TestModel():

    """Class used to test the Model class."""

    def set_up(self):
        """Method to setup the parameters needed during the tests."""
        # connect to the database
        # self.connection = engine.connect()

        # begin a non-ORM transaction
        # self.trans = self.connection.begin()

        # bind an individual Session to the connection
        # self.session = Session(bind=self.connection)

    def test_password_matches(self):
        """
        Send a raw password to be hashed.


        Compare returned hash to verify that can

        """

        password_raw = "password"
        # I do not understand why I have to pass an extra argument
        # just to make this work.

        password_hash = project.classes.model.hash_password(
                              password=password_raw)

        assert project.classes.model.verify_password(
            password_hash, password_raw) is True

    def test_password_not_match(self):
        """

        Test that the algorithm correctly identifies when.

        passwords do not match.

        """

        password_raw = "password"
        password_wrong = "wrong password"
        password_hash = project.classes.model.hash_password(password_raw)

        assert project.classes.model.verify_password(
                                           password_hash,
                                           password_wrong) is False

    def tearDown(self):
        self.session.close()

        # rollback - everything that happened with the
        # Session above (including calls to commit())
        # is rolled back.
        self.trans.rollback()

        # return connection to the Engine
        self.connection.close()
